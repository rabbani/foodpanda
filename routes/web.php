<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/upload_file', [App\Http\Controllers\PostController::class, 'create'])->name('upload_file');
//Route::post('/post_store', [App\Http\Controllers\PostController::class, 'store'])->name('post.store');
Route::post('/import_excel', [App\Http\Controllers\PostController::class, 'import']);
