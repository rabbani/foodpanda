<?php

namespace App\Http\Controllers;

use App\Imports\ProjectsImport;
use App\Models\Post;
use App\Models\Project;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form');
    }

    function import(Request $request)
    {
//        $this->validate($request, [
//            'select_file'  => 'required|mimes:xls,xlsx'
//        ]);
        Excel::import(new ProjectsImport, \request()->file('select_file'));

        return back()->with('success', 'Excel Data Imported successfully.');
    }

    public function store(Request $request)
    {


            $path = $request->file('import_file')->getRealPath();
dd($path);
            $data = Excel::load($path)->get();

            if($data->count() > 0)
            {
                foreach($data->toArray() as $key => $value)
                {
                    foreach($value as $row)
                    {
                        $insert_data[] = array(
                            'CustomerName'  => $row['customer_name'],
                            'Gender'   => $row['gender'],
                            'Address'   => $row['address'],
                            'City'    => $row['city'],
                            'PostalCode'  => $row['postal_code'],
                            'Country'   => $row['country']
                        );
                    }
                }

                if(!empty($insert_data))
                {
                    DB::table('tbl_customer')->insert($insert_data);
                }
            }
            return back()->with('success', 'Excel Data Imported successfully.');


//        Session::put('success', 'Your file successfully import in database!!!');
//
//        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
