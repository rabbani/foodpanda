<?php

namespace App\Imports;

use App\Models\Post;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class ProjectsImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        return new Post([
            'product_name' => $row['product_title_en_bd'],
            'unit'    => $row['units'],
            'price'    => $row['price_per_pack_total'],
            'quantity'    => $row['sizeweight'],
            'image'    => $row['image_1_url'],
            'category'    => $row['category'],
            'description'    => $row['product_description_en_bd'],

        ]);
    }
}
