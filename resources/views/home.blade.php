@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($posts as $post)
        <div class="col-md-3 mb-2">
            <div class="card" style="width: 18rem;">
                <img src="{{$post->image}}" class="card-img-top" alt="image" style="height: 18em;">
                <div class="card-body">
                    <h5 class="card-title">{{$post->product_name}}</h5>
                    <div class="d-flex justify-content-between">
                        <p class="card-text">BDT {{$post->price}}</p>
                        <p class="card-text">{{$post->quantity}} {{$post->unit}}</p>
                    </div>
                    <p class="card-text">{{$post->category}}</p>
                    <p class="card-text">{{$post->description}}</p>
                </div>
            </div>
        </div>
        @endforeach
            {!! $posts->render() !!}
    </div>
</div>
@endsection
