@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container">
            <h3 align="center">Import Excel File in Laravel</h3>
            <br />
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    Upload Validation Error<br><br>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <form method="post" enctype="multipart/form-data" action="{{ url('/import_excel') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <table class="table">
                        <tr>
                            <td width="40%" align="right"><label>Select File for Upload</label></td>
                            <td width="30">
                                <input type="file" name="select_file" />
                            </td>
                            <td width="30%" align="left">
                                <input type="submit" name="upload" class="btn btn-primary" value="Upload">
                            </td>
                        </tr>
                        <tr>
                            <td width="40%" align="right"></td>
                            <td width="30"><span class="text-muted">.xls, .xslx</span></td>
                            <td width="30%" align="left"></td>
                        </tr>
                    </table>
                </div>
            </form>

            <br />
{{--            <div class="panel panel-default">--}}
{{--                <div class="panel-heading">--}}
{{--                    <h3 class="panel-title">Customer Data</h3>--}}
{{--                </div>--}}
{{--                <div class="panel-body">--}}
{{--                    <div class="table-responsive">--}}
{{--                        <table class="table table-bordered table-striped">--}}
{{--                            <tr>--}}
{{--                                <th>Customer Name</th>--}}
{{--                                <th>Gender</th>--}}
{{--                                <th>Address</th>--}}
{{--                                <th>City</th>--}}
{{--                                <th>Postal Code</th>--}}
{{--                                <th>Country</th>--}}
{{--                            </tr>--}}
{{--                            @foreach($data as $row)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $row->CustomerName }}</td>--}}
{{--                                    <td>{{ $row->Gender }}</td>--}}
{{--                                    <td>{{ $row->Address }}</td>--}}
{{--                                    <td>{{ $row->City }}</td>--}}
{{--                                    <td>{{ $row->PostalCode }}</td>--}}
{{--                                    <td>{{ $row->Country }}</td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-8">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header">{{ __('Upload File') }}</div>--}}

{{--                    <div class="card-body">--}}
{{--                        <form action="post_store" method="post" enctype="multipart/form-data">--}}
{{--                            {{ csrf_field() }}--}}
{{--                        <div class="mb-3">--}}
{{--                            <label for="formFile" class="form-label">Select Excel File</label>--}}
{{--                            <input class="form-control" type="file" id="formFile" name="import_file">--}}
{{--                        </div>--}}
{{--                        <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
@endsection
